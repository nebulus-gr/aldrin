import { AldrinApiPoolsClient } from "../../src"; // or "@aldrin-exchange/sdk"
const fs = require("fs");

export async function getTotalVolumeLocked() {
  const client = new AldrinApiPoolsClient();

  // const tvl = await client.getTotalVolumeLocked();
  // console.log("TVL: ", tvl);

  const poolsInfo = await client.getPoolsInfo();

  const data = [];

  poolsInfo.forEach((el) => {
    const { name, tvl } = el;
    if (name === "mSOL_USDC") {
      const base = tvl.base.toString(10);
      const quote = tvl.quote.toString(10);
      console.log(
        "base",
        base,
        "quote",
        quote,
        "price",
        parseFloat(quote) / parseFloat(base)
      );
    }
  });
}

getTotalVolumeLocked();
