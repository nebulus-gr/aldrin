import { PublicKey } from "@solana/web3.js";
import { AldrinApiPoolsClient, AUTHORIZED_POOLS, TokenSwap } from "../../src"; // or "@aldrin-exchange/sdk"
const fs = require("fs");

export async function main() {
  const tokenSwap = await TokenSwap.initialize();

  const client = new AldrinApiPoolsClient();

  // const tvl = await client.getTotalVolumeLocked();
  // console.log("TVL: ", tvl);

  const poolsInfo = await client.getPoolsInfo();

  const price = await tokenSwap.getAllPrice();
  console.log("price: ", price);
}

main();
