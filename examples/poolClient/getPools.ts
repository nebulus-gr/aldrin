import { AUTHORIZED_POOLS, PERMISSIONLESS_POOLS } from "../../src"; // or "@aldrin-exchange/sdk"
import { poolClient } from "../common";

export async function getPools() {
  console.log("Predefined authorized pools: ", AUTHORIZED_POOLS);
  console.log("Predefined permissionless pools: ", PERMISSIONLESS_POOLS);
  const pools = await poolClient.getPools();
  console.log("All Aldrin pools: ", pools[0]);

  const v2pools = await poolClient.getV2Pools();
  console.log("All Aldrin v2 pools: ", v2pools[0]);
}

getPools();
